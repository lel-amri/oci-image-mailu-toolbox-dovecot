# syntax=docker.io/docker/dockerfile:1.3
FROM docker.io/library/alpine:3.17.2 AS final
RUN set -eux ; \
	apk add --no-cache tar bind-tools xz gzip bzip2 curl
RUN set -eux ; \
	apk add --no-cache dovecot
